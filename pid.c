/*
 * pid - find the pid of a process given its command name
 *
 * Same basic idea as Debian's "pidof", in that you type 'pid command'
 * and it finds a process running that command and gives you the pid;
 * but differs in details, for example it will search for scripts by
 * default rather than requiring pidof's -x option, and it will also
 * look for command-line arguments ('pid make test') and try to find
 * the parent process of a bunch of forks from the same shell script
 * invocation.
 *
 * Currently tested only on Linux using /proc directly, but I've tried
 * to set it up so that the logic of what processes to choose is
 * separated from the mechanism used to iterate over processes and
 * find their command lines.
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <assert.h>
#include <ctype.h>

#include <sys/types.h>
#include <dirent.h>
#include <unistd.h>

#define lenof(x) (sizeof((x))/sizeof(*(x)))

/* ----------------------------------------------------------------------
 * General-purpose code for storing a set of process ids, testing
 * membership, and iterating over them.
 *
 * I do this with a splay tree, because that automatically speeds up
 * from N log N to linear-time performance while inserting things in
 * numerical order (as get_processes does) or iterating over the tree.
 * The downside is that the N log N guarantee the rest of the time is
 * only amortised rather than worst-case per individual operation -
 * but when we're not doing anything interactive between operations,
 * that's good enough.
 */

struct pidnode {
    unsigned pid;
    struct pidnode *parent, *child[2];
};

static struct pidnode *pidnode_insert(struct pidnode **root,
                                      struct pidnode *node)
{
    int ci;

    if (!*root) {
        node->parent = node->child[0] = node->child[1] = NULL;
        *root = node;
        return NULL;
    }

    if ((*root)->pid == node->pid) {
        return *root;
    }

    ci = ((*root)->pid < node->pid);
    node = pidnode_insert(&(*root)->child[ci], node);
    (*root)->child[ci]->parent = (*root);
    return node;
}

static int pidnode_which_child(struct pidnode *node)
{
    assert(node);
    assert(node->parent);
    if (node->parent->child[0] == node) {
        return 0;
    } else {
        assert(node->parent->child[1] == node);
        return 1;
    }
}

static struct pidnode *pidnode_rotate(struct pidnode **root,
                                      struct pidnode *node)
{
    int ci = pidnode_which_child(node);
    struct pidnode *parent = node->parent;
    struct pidnode *grandparent = parent->parent;

    if (grandparent) {
        grandparent->child[pidnode_which_child(parent)] = node;
        node->parent = grandparent;
    } else {
        assert(*root == parent);
        *root = node;
        node->parent = NULL;
    }

    if ((parent->child[ci] = node->child[1-ci]) != NULL)
        parent->child[ci]->parent = parent;

    node->child[1-ci] = parent;
    parent->parent = node;

    return node;
}

static struct pidnode *pidnode_splay(struct pidnode **root,
                                     struct pidnode *node)
{
    if (!node || !node->parent)
        return node;

    while (node->parent && node->parent->parent) {
        if (pidnode_which_child(node) == pidnode_which_child(node->parent)) {
            pidnode_rotate(root, node->parent);
        } else {
            pidnode_rotate(root, node);
        }
        pidnode_rotate(root, node);
    }

    if (node->parent)
        pidnode_rotate(root, node);

    return node;
}

static struct pidnode *pidnode_query(struct pidnode *node, unsigned pid)
{
    while (node && pid != node->pid)
        node = node->child[pid > node->pid];

    return node;
}

static struct pidnode *pidnode_query_ge(struct pidnode *node, unsigned pid)
{
    struct pidnode *last_went_left = NULL;

    while (node) {
        if (pid > node->pid) {
            node = node->child[1];
        } else {
            last_went_left = node;
            node = node->child[0];
        }
    }

    node = last_went_left;
    while (node && node->child[0])
        node = node->child[0];
    return node;
}

#ifdef VALIDATE_TREE
static void pidnode_validate(struct pidnode *node, struct pidnode *parent,
                             unsigned *min, unsigned *max,
                             int depth)
{
    if (!node)
        return;
    printf("%*svalidate %p [%d]\n", depth*2, "", node, node->pid);
    assert(!min || node->pid > *min);
    assert(!max || node->pid < *max);
    pidnode_validate(node->child[0], node, min, &node->pid, depth+1);
    pidnode_validate(node->child[1], node, &node->pid, max, depth+1);
}
#else
#define pidnode_validate(a,b,c,d,e) ((void)0)
#endif

static void pidnode_free(struct pidnode *root)
{
    if (!root)
        return;
    pidnode_free(root->child[0]);
    pidnode_free(root->child[1]);
    free(root);
}

struct pidset {
    struct pidnode *root;
    int count;
};

#define PIDSET_INIT { NULL, 0 }

static void pidset_clear(struct pidset *p)
{
    pidnode_free(p->root);
    p->root = NULL;
    p->count = 0;
}

static void pidset_add(struct pidset *p, int pid)
{
    int unused = 0;
    struct pidnode *node, *prevnode;
    node = malloc(sizeof(struct pidnode));
    if (!node) {
        fprintf(stderr, "pid: out of memory\n");
        exit(1);
    }
    node->pid = pid;
    prevnode = pidnode_insert(&p->root, node);
    if (prevnode) {
        free(node);
        pidnode_splay(&p->root, prevnode);
    } else {
        p->count++;
        pidnode_splay(&p->root, node);
    }
    pidnode_validate(p->root, NULL, NULL, NULL, 0);
}

static int pidset_in(struct pidset *p, int pid)
{
    struct pidnode *node = pidnode_query(p->root, pid);
    pidnode_splay(&p->root, node);
    pidnode_validate(p->root, NULL, NULL, NULL, 0);
    return node != NULL;
}

static int pidset_size(struct pidset *p)
{
    return p->count;
}

static int pidset_first(struct pidset *p)
{
    struct pidnode *node = pidnode_splay(
        &p->root, pidnode_query_ge(p->root, 0));
    pidnode_validate(p->root, NULL, NULL, NULL, 0);
    return node ? node->pid : -1;
}

static int pidset_next(struct pidset *p, int pid)
{
    struct pidnode *node = pidnode_splay(
        &p->root, pidnode_query_ge(p->root, pid+1));
    pidnode_validate(p->root, NULL, NULL, NULL, 0);
    return node ? node->pid : -1;
}

static void pidset_swap(struct pidset *p, struct pidset *q)
{
    struct pidnode *ntmp;
    int ctmp;

    ntmp = p->root; p->root = q->root; q->root = ntmp;
    ctmp = p->count; p->count = q->count; q->count = ctmp;
}

/* ----------------------------------------------------------------------
 * Code to scan the list of processes and retrieve all the information
 * we'll want about each one. This may in future be conditional on the
 * OS's local mechanism for finding that information (i.e. if we want
 * to run on kernels that don't provide Linux-style /proc).
 */

struct procdata {
    int pid, ppid, uid;
    int argc;
    const char *const *argv;
    const char *exe;
};

struct procnode {
    struct pidnode node;
    struct procdata pd;
};

static struct pidnode *procdata_root = NULL;

static void store_procdata(struct procdata *pd_in)
{
    struct procnode *pn;
    struct pidnode *prevnode;

    pn = malloc(sizeof(struct procnode));
    if (!pn) {
        fprintf(stderr, "pid: out of memory\n");
        exit(1);
    }
    pn->node.pid = pd_in->pid;
    prevnode = pidnode_insert(&procdata_root, &pn->node);
    assert(!prevnode);
    pidnode_splay(&procdata_root, &pn->node);
    pidnode_validate(procdata_root, NULL, NULL, NULL, 0);
    pn->pd = *pd_in;
}

static char *get_contents(const char *filename, int *returned_len)
{
    int len;
    char *buf = NULL;
    int bufsize = 0;

    FILE *fp = fopen(filename, "rb");
    if (!fp)
        return NULL;

    len = 0;
    while (1) {
        int readret;

        if (len >= bufsize) {
            bufsize = len * 5 / 4 + 4096;
            buf = realloc(buf, bufsize);
            if (!buf) {
                fprintf(stderr, "pid: out of memory\n");
                exit(1);
            }
        }

        readret = fread(buf + len, 1, bufsize - len, fp);
        if (readret < 0) {
            fclose(fp);
            free(buf);
            return NULL;               /* I/O error */
        } else if (readret == 0) {
            fclose(fp);
            if (returned_len)
                *returned_len = len;
            buf = realloc(buf, len + 1);
            buf[len] = '\0';
            return buf;
        } else {
            len += readret;
        }
    }
}

static char *get_link_dest(const char *filename)
{
    char *buf;
    int bufsize;
    ssize_t ret;

    buf = NULL;
    bufsize = 0;

    while (1) {
        bufsize = bufsize * 5 / 4 + 1024;
        buf = realloc(buf, bufsize);
        if (!buf) {
            fprintf(stderr, "pid: out of memory\n");
            exit(1);
        }

        ret = readlink(filename, buf, (size_t)bufsize);
        if (ret < 0) {
            free(buf);
            return NULL;               /* I/O error */
        } else if (ret < bufsize) {
            /*
             * Success! We've read the full link text.
             */
            buf = realloc(buf, ret+1);
            buf[ret] = '\0';
            return buf;
        } else {
            /* Overflow. Go round again. */
        }
    }
}

static void get_processes(struct pidset *out)
{
    struct dirent *de;
    DIR *d;

    pidset_clear(out);

    d = opendir("/proc");
    if (!d) {
        perror("/proc: open\n");
        exit(1);
    }
    while ((de = readdir(d)) != NULL) {
        int pid;
        char *cmdline, *status, *exe;
        int cmdlinelen;
        const char **argv;
        char filename[256];
        struct procdata *proc;

        const char *name = de->d_name;
        if (name[strspn(name, "0123456789")])
            continue;

        /*
         * The filename is numeric, i.e. we've found a pid. Try to
         * retrieve all the information we want about it.
         *
         * We expect this will fail every so often for random reasons,
         * e.g. if the pid has disappeared between us fetching a list
         * of them and trying to read their command lines. In that
         * situation, we won't bother reporting errors: we'll just
         * drop this pid and silently move on to the next one.
         */
        pid = atoi(name);
        assert(pid >= 0);

        sprintf(filename, "/proc/%d/cmdline", pid);
        if ((cmdline = get_contents(filename, &cmdlinelen)) == NULL)
            continue;

        sprintf(filename, "/proc/%d/status", pid);
        if ((status = get_contents(filename, NULL)) == NULL) {
            free(cmdline);
            continue;
        }

        sprintf(filename, "/proc/%d/exe", pid);
        exe = get_link_dest(filename);
        /* This may fail, if the process isn't ours, but we continue
         * anyway. */

        /*
         * Now we've got all our raw data out of /proc. Process it
         * into the internal representation we're going to use in the
         * process-selection logic.
         */
        proc = (struct procdata *)malloc(sizeof(struct procdata));
        if (!proc) {
            fprintf(stderr, "pid: out of memory\n");
            exit(1);
        }
        proc->pid = pid;
        proc->exe = exe;

        /*
         * cmdline contains a list of NUL-terminated strings. Scan
         * them to get the argv pointers.
         */
        {
            const char *p;
            int nargs;

            /* Count the arguments. */
            nargs = 0;
            for (p = cmdline; p < cmdline + cmdlinelen; p += strlen(p)+1)
                nargs++;

            /* Allocate space for the pointers. */
            argv = (const char **)malloc((nargs+1) * sizeof(char *));
            proc->argv = argv;
            if (!argv) {
                fprintf(stderr, "pid: out of memory\n");
                exit(1);
            }

            /* Store the pointers. */
            proc->argc = 0;
            for (p = cmdline; p < cmdline + cmdlinelen; p += strlen(p)+1)
                argv[proc->argc++] = p;

            /* Trailing NULL to match standard argv lists, just in case. */
            assert(proc->argc == nargs);
            argv[proc->argc] = NULL;
        }

        /*
         * Scan status for the uid and the parent pid. This file
         * contains a list of \n-terminated lines of text.
         */
        {
            const char *p;
            int got_ppid = 0, got_uid = 0;

            p = status;
            while (p && *p) {
                if (!got_ppid && sscanf(p, "PPid: %d", &proc->ppid) == 1)
                    got_ppid = 1;
                if (!got_uid && sscanf(p, "Uid: %*d %d", &proc->uid) == 1)
                    got_uid = 1;

                /*
                 * Advance to next line.
                 */
                p = strchr(p, '\n');
                if (p) p++;
            }

            if (!got_uid || !got_ppid) { /* arrgh, abort everything so far */
                free(cmdline);
                free(exe);
                free(status);
                free(argv);
                free(proc);
                continue;
            }
        }

        /*
         * If we get here, we've got everything we need. Add the
         * process to the list of things we can usefully work
         * with.
         */
        store_procdata(proc);
        pidset_add(out, pid);
    }
    closedir(d);
}

static const struct procdata *get_proc(int pid)
{
    struct pidnode *node = pidnode_query(procdata_root, pid);
    assert(node);
    pidnode_splay(&procdata_root, node);
    pidnode_validate(procdata_root, NULL, NULL, NULL, 0);
    return &((struct procnode *)node)->pd;
}

/* ----------------------------------------------------------------------
 * Logic to pick out the set of processes we care about.
 */

static int is_an_interpreter(const char *basename, const char **stop_opt)
{
    if (!strcmp(basename, "perl") ||
        !strcmp(basename, "ruby")) {
        *stop_opt = "-e";
        return 1;
    }
    if (!strcmp(basename, "python") ||
        !strcmp(basename, "python3") ||
        !strcmp(basename, "bash") ||
        !strcmp(basename, "sh") ||
        !strcmp(basename, "dash")) {
        *stop_opt = "-c";
        return 1;
    }
    if (!strcmp(basename, "rep") ||
        !strcmp(basename, "lua") ||
        !strcmp(basename, "java") ||
        !strcmp(basename, "wish") ||
        !strcmp(basename, "tclsh")) {
        *stop_opt = NULL;
        return 1;
    }
    return 0;
}

static const char *find_basename(const char *path)
{
    const char *ret = path;
    const char *p;

    while (1) {
        p = ret + strcspn(ret, "/");
        if (*p) {
            ret = p+1;
        } else {
            return ret;
        }
    }
}

static int debug = 0;
static void diagnose(const char *fmt, ...)
{
    if (debug) {
        va_list ap;
        va_start(ap, fmt);
        vfprintf(stderr, fmt, ap);
        va_end(ap);
        fputc('\n', stderr);
    }
}

static int find_command(int pid_argc, const char *const *pid_argv,
                        const char *cmd, int pid)
{
    const char *base, *stop_opt;

    base = pid_argv[0];
    if (*base == '-')
        base++;                   /* skip indicator of login shells */
    base = find_basename(base);

    diagnose("find command name for pid %d:"
             " full argv[0] = '%s', basename = '%s'",
             pid, pid_argv[0], base);

    if (!strcmp(base, cmd)) {
        /*
         * argv[0] matches the supplied command name.
         */
        return 0;
    } else if (is_an_interpreter(base, &stop_opt)) {
        /*
         * argv[0] is an interpreter program of some kind. Look
         * along its command line for the program it's running,
         * and see if _that_ matches the command name.
         */
        int i = 1;
        diagnose("  known interpreter, looking for script name");
        while (i < pid_argc && pid_argv[i][0] == '-') {
            /*
             * Skip interpreter options, unless they're things which
             * make the next non-option argument not a script name
             * (e.g. sh -c, perl -e).
             */
            if (stop_opt && !strncmp(pid_argv[i], stop_opt,
                                     strlen(stop_opt))) {
                diagnose("  argv[%d] = '%s' has non-script-name marker "
                         "option '%s'", i, pid_argv[i], stop_opt);
                return -1;             /* no match */
            }
            i++;
        }
        if (i < pid_argc) {
            base = find_basename(pid_argv[i]);
            diagnose("  full argv[%d] = '%s', basename = '%s'",
                     i, pid_argv[i], base);
            if (!strcmp(base, cmd))
                return i;
        } else {
            diagnose("  no script name found");
        }
    }
    return -1;                         /* no match */
}

static int strnullcmp(const char *a, const char *b)
{
    /*
     * Like strcmp, but cope with NULL inputs by making them compare
     * identical to each other and before any non-null string.
     */
    if (!a || !b)
        return (b != 0) - (a != 0);
    else
        return strcmp(a, b);
}

static int argcmp(const char *const *a, const char *const *b)
{
    while (*a && *b) {
        int ret = strcmp(*a, *b);
        if (ret)
            return ret;
        a++;
        b++;
    }

    return (*b != NULL) - (*a != NULL);
}

static void filter_out_self(struct pidset *out, struct pidset *in)
{
    /*
     * Discard our own pid from a set. (Generally we won't want to
     * return ourself from any search.)
     */
    int pid;
    int our_pid = getpid();

    pidset_clear(out);
    for (pid = pidset_first(in); pid >= 0; pid = pidset_next(in, pid)) {
        if (pid == our_pid) {
            diagnose("excluding our own pid %d", pid);
        } else {
            pidset_add(out, pid);
        }
    }
}

static void filter_by_uid(struct pidset *out, struct pidset *in, int uid)
{
    /*
     * Return only those processes with a given uid.
     */
    int pid;

    pidset_clear(out);
    for (pid = pidset_first(in); pid >= 0; pid = pidset_next(in, pid)) {
        const struct procdata *proc = get_proc(pid);
        if (proc->uid == uid) {
            pidset_add(out, pid);
        } else {
            diagnose("excluding pid %d whose uid %d is not our uid %d",
                     pid, proc->uid, uid);
        }
    }
}

static void filter_by_command(
    struct pidset *out, struct pidset *in, const char **words)
{
    /*
     * Look for processes matching the user-supplied command name and
     * subsequent arguments.
     */
    int pid;

    pidset_clear(out);
    for (pid = pidset_first(in); pid >= 0; pid = pidset_next(in, pid)) {
        const struct procdata *proc = get_proc(pid);
        int i, j;

        if (!proc->argv || proc->argc < 1) {
            diagnose("excluding pid %d which has no argv[0]", pid);
            goto no_match;
        }

        /* Find the command, whether it's a binary or a script. */
        i = find_command(proc->argc, proc->argv, words[0], pid);
        if (i < 0) {
            diagnose("excluding pid %d whose command doesn't match '%s'",
                     pid, words[0]);
            goto no_match;
        }

        /* Now check that subsequent arguments match. */
        for (j = 1; words[j]; j++) {
            if (!proc->argv[i+j] || strcmp(proc->argv[i+j], words[j])) {
                diagnose("excluding pid %d whose argv[%d] is %s%s%s, not '%s'",
                         pid, i+j,
                         proc->argv[i+j] ? "'" : "",
                         proc->argv[i+j] ? proc->argv[i+j] : "NULL",
                         proc->argv[i+j] ? "'" : "",
                         words[j]);
                goto no_match;
            }
        }

        /* If we get here, we have a match! */
        pidset_add(out, pid);

      no_match:;
    }
}

static void filter_out_forks(struct pidset *out, struct pidset *in)
{
    /*
     * Discard any process whose parent is also in our remaining match
     * set and looks sufficiently like it for us to decide this one's
     * an uninteresting fork (e.g. of a shell script executing a
     * complex pipeline).
     */
    int pid;

    pidset_clear(out);
    for (pid = pidset_first(in); pid >= 0; pid = pidset_next(in, pid)) {
        const struct procdata *proc = get_proc(pid);

        if (pidset_in(in, proc->ppid)) {
            /* The parent is in our set too. Is it similar? */
            const struct procdata *parent = get_proc(proc->ppid);
            if (!strnullcmp(parent->exe, proc->exe) &&
                !argcmp(parent->argv, proc->argv)) {
                /* Yes; don't list it. */
                continue;
            }
        }

        pidset_add(out, pid);
    }
}

/* ----------------------------------------------------------------------
 * Main program.
 */

const char usagemsg[] =
    "usage: pid [options] <search-cmd> [<search-arg>...]\n"
    "where: -a                 report all matching pids, not just one\n"
    "       -U                 report pids of any user, not just ours\n"
    " also: pid --version      report version number\n"
    "       pid --help         display this help text\n"
    "       pid --licence      display the (MIT) licence text\n"
    ;

void usage(void) {
    fputs(usagemsg, stdout);
}

const char licencemsg[] =
    "pid is copyright 2012 Simon Tatham.\n"
    "\n"
    "Permission is hereby granted, free of charge, to any person\n"
    "obtaining a copy of this software and associated documentation files\n"
    "(the \"Software\"), to deal in the Software without restriction,\n"
    "including without limitation the rights to use, copy, modify, merge,\n"
    "publish, distribute, sublicense, and/or sell copies of the Software,\n"
    "and to permit persons to whom the Software is furnished to do so,\n"
    "subject to the following conditions:\n"
    "\n"
    "The above copyright notice and this permission notice shall be\n"
    "included in all copies or substantial portions of the Software.\n"
    "\n"
    "THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND,\n"
    "EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF\n"
    "MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND\n"
    "NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS\n"
    "BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN\n"
    "ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN\n"
    "CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE\n"
    "SOFTWARE.\n"
    ;

void licence(void) {
    fputs(licencemsg, stdout);
}

void version(void) {
    printf("pid, unknown version\n"); /*---buildsys-replace---*/
}

int main(int argc, char **argv)
{
    const char **searchwords;
    int nsearchwords;
    int all = 0, all_uids = 0;
    int doing_opts = 1;

    /*
     * Allocate enough space in 'searchwords' that we could shovel the
     * whole of our argv into it if we had to. Then we won't have to
     * worry about it later.
     */
    searchwords = (const char **)malloc((argc+1) * sizeof(const char *));
    nsearchwords = 0;

    /*
     * Parse the command line.
     */
    while (--argc > 0) {
        char *p = *++argv;
        if (doing_opts && *p == '-') {
            if (!strcmp(p, "-a") || !strcmp(p, "--all")) {
                all = 1;
            } else if (!strcmp(p, "-U") || !strcmp(p, "--all-uids")) {
                all_uids = 1;
            } else if (!strcmp(p, "-D") || !strcmp(p, "--debug")) {
                debug = 1;
            } else if (!strcmp(p, "--version")) {
                version();
                return 0;
            } else if (!strcmp(p, "--help")) {
                usage();
                return 0;
            } else if (!strcmp(p, "--licence") || !strcmp(p, "--license")) {
                licence();
                return 0;
            } else if (!strcmp(p, "--")) {
                doing_opts = 0;
            } else {
                fprintf(stderr, "pid: unrecognised option '%s'\n", p);
                return 1;
            }
        } else {
            searchwords[nsearchwords++] = p;
            doing_opts = 0; /* further optionlike args become search terms */
        }
    }

    if (!nsearchwords) {
        fprintf(stderr, "pid: expected a command to search for; "
                "type 'pid --help' for help\n");
        return 1;
    }
    searchwords[nsearchwords] = NULL;  /* terminate list */

    {
        struct pidset procs = PIDSET_INIT;
        struct pidset ptmp = PIDSET_INIT;
        int uid, pid, npids;
        int status = 0;

        /*
         * Construct our list of processes.
         */
        get_processes(&procs);
        uid = getuid();
        if (uid > 0 && !all_uids) {
            filter_by_uid(&ptmp, &procs, uid);
            pidset_swap(&procs, &ptmp);
        }

        filter_out_self(&ptmp, &procs);
        pidset_swap(&procs, &ptmp);

        filter_by_command(&ptmp, &procs, searchwords);
        pidset_swap(&procs, &ptmp);

        if (!all) {
            filter_out_forks(&ptmp, &procs);
            pidset_swap(&procs, &ptmp);
        }

        /*
         * Output.
         */
        npids = pidset_size(&procs);
        if (npids == 0) {
            printf("NONE\n");
            status = 1;
        } else if (all) {
            const char *sep = "";
            for (pid = pidset_first(&procs); pid >= 0;
                 pid = pidset_next(&procs, pid)) {
                printf("%s%d", sep, pid);
                sep = " ";
            }
            putchar('\n');
        } else {
            if (npids == 1) {
                printf("%d\n", pidset_first(&procs));
            } else {
                if (debug) {
                    diagnose("%d pids remaining:", npids);
                    for (pid = pidset_first(&procs); pid >= 0;
                         pid = pidset_next(&procs, pid))
                        diagnose("  %d", pid);
                }
                printf("MULTIPLE\n");
                status = 2;
            }
        }

        pidset_clear(&procs);
        pidset_clear(&ptmp);

        return status;
    }
}
